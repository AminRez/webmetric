//
//  TasksViewModel.swift
//  WebMetric
//
//  Created by Amin Rezaei on 4/25/1402 AP.
//

import Foundation

class TasksViewModel: ObservableObject {
    // Everything that gets adjusted and needs UI update has to be marked published
    @Published var data: [TasksModel] = [
        TasksModel(taskName: "Sample Task 1", dueDate: "1 Jan", description: "Subject", completed: true),
        TasksModel(taskName: "Sample Task 2", dueDate: "2 Jan", description: "Subject B", completed: false),
        TasksModel(taskName: "Sample Task 3", dueDate: "3 Jan", description: "Subject C", completed: false)
    ]
}
