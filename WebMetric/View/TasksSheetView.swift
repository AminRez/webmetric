//
//  TasksSheetView.swift
//  WebMetric
//
//  Created by Amin Rezaei on 4/25/1402 AP.
//

import SwiftUI

struct TasksSheetView: View {
    
    @Environment(\.dismiss) var dismiss
    
    @Binding var taskName: String
    @Binding var taskDate: String
    @Binding var taskDescription: String
    @Binding var taskAdded: Bool
    
    var body: some View {
        NavigationView {
            VStack {
                
                HStack {
                    Text("Enter the task's name:")
                        .padding(.horizontal)
                    Spacer()
                }
                
                HStack {
                    
                    TextField("Name",text: $taskName)
                        .frame(width: 300)
                        .overlay(VStack{Divider().offset(x: 0, y: 15)})
                        .padding()
                    
                    
                }
                
                HStack {
                    Text("Enter description:")
                        .padding(.horizontal)
                    Spacer()
                }
                
                HStack {
                    
                    TextField("Description",text: $taskDescription)
                        .frame(width: 300)
                        .overlay(VStack{Divider().offset(x: 0, y: 15)})
                        .padding()
                    
                    
                }
                
                HStack {
                    Text("Enter due Date:")
                        .padding(.horizontal)
                    Spacer()
                }
                
                HStack {
                    
                    TextField("Date: 13 jan",text: $taskDate)
                        .frame(width: 300)
                        .overlay(VStack{Divider().offset(x: 0, y: 15)})
                        .padding()
                    
                    
                }
                
                Spacer()
            }.padding(.top)
            
            .navigationTitle("New Task")
            
            .toolbar {
                
                ToolbarItem(placement: .navigationBarTrailing) {
                    
                    Button {
                        taskAdded = true
                        
                        taskName = ""
                        taskDate = ""
                        taskDescription = ""
                        
                        dismiss()
                    } label: {
                        Text("Done")
                    }
                    .padding()

                }
            }
        }
    }
}

//struct TasksSheetView_Previews: PreviewProvider {
//    static var previews: some View {
//        TasksSheetView()
//    }
//}
