//
//  ContentView.swift
//  WebMetric
//
//  Created by Amin Rezaei on 4/25/1402 AP.
//

import SwiftUI

struct TasksView: View {
    
    // Observed to update you UI
    @ObservedObject var assignment = TasksViewModel()
    
    @State private var showingSheet = false
    @State var isOn = false
    @State var taskAdded = false
    
    @State var taskName = ""
    @State var taskDate = ""
    @State var taskDescription = ""
    
    let timer = Timer.publish(every: 1, on: .current, in: .common).autoconnect()
    
    var body: some View {
        NavigationView {
            VStack {
                Toggle("Show Completed tasks", isOn: $isOn) // 2
                            .padding()

                List {
                    Section(header: isOn ? Text("Completed") : Text("Not Completed")) {
                        ForEach(self.assignment.data, id: \.self) { task in
            //                        HStack {
                            if task.completed && isOn {
                                NavigationLink (
                                    destination: EmptyView(),
                                    label: {
                                        Image(systemName: "doc.append.fill")
                                            .scaleEffect(2.5)
                                            .padding()
                                        
                                        VStack(alignment: .leading, spacing: 3) {
                                            
                                            Text(task.taskName)
                                                .fontWeight(.semibold)
                                                .lineLimit(2)
                                            
                                            Text(task.dueDate)
                                                .font(.subheadline)
                                                .foregroundColor(.secondary)
                                        }
                                    })
                            } else if !task.completed && !isOn {
                                NavigationLink (
                                    destination: EmptyView(),
                                    label: {
                                        Image(systemName: "doc.append.fill")
                                            .scaleEffect(2.5)
                                            .padding()
                                        
                                        VStack(alignment: .leading, spacing: 3) {
                                            
                                            Text(task.taskName)
                                                .fontWeight(.semibold)
                                                .lineLimit(2)
                                            
                                            Text(task.dueDate)
                                                .font(.subheadline)
                                                .foregroundColor(.secondary)
                                        }
                                    })
                            }
                                   
                                }.onDelete { IndexSet in
                                    assignment.data.remove(atOffsets: IndexSet)
                                }
                                .onMove {
                                    assignment.data.move(fromOffsets: $0, toOffset: $1)
                            }
                                .onReceive(timer) { a in
                                    if taskAdded {
                                        print(4)
                                    }
                                }
                    }
                            
                        }
                .sheet(isPresented: $showingSheet) {
                    TasksSheetView(taskName: $taskName, taskDate: $taskDate, taskDescription: $taskDescription,taskAdded: $taskAdded)
                }
                
                .navigationTitle("My Tasks")
                .toolbar {

                    ToolbarItem(placement: .navigationBarTrailing) {
                        HStack {
                            
                            EditButton()
                                .foregroundColor(.black)
                            
                            Button {
                                showingSheet.toggle()
                            } label: {
                                VStack {
                                    Image(systemName: "plus.circle")
                                }.foregroundColor(.black)
                            }
                            
                        }.padding()

                    }
            }
            }
        }
        .onAppear {
            let _ = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { _ in
                if taskAdded {
                    print("2")
                    addData(name: taskName, date: taskDate, description: taskDescription)
                    taskAdded = false
                }
            }
            
        }
        
    }
    
    func addData(name: String,date: String, description: String) {
        self.assignment.data.append(TasksModel(taskName: name, dueDate: date, description: description, completed: false))
        
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        TasksView()
    }
}
