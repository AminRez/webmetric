//
//  TasksModel.swift
//  WebMetric
//
//  Created by Amin Rezaei on 4/25/1402 AP.
//

import Foundation

struct TasksModel: Hashable, Identifiable {
    var id: String = UUID().uuidString
    var taskName: String
    var dueDate: String
    var description: String
    var completed: Bool
}
