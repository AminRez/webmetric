//
//  WebMetricApp.swift
//  WebMetric
//
//  Created by Amin Rezaei on 4/25/1402 AP.
//

import SwiftUI

@main
struct WebMetricApp: App {
    var body: some Scene {
        WindowGroup {
            TasksView()
        }
    }
}
